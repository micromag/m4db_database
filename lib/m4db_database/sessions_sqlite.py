r"""
A set of utilities to create/open sqlite databases for SQLAlchemy.

@file util.sqlite.py
@author L. Nagy, W. Williams
"""
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

import m4db_database.orm

from m4db_database.configuration import read_config_from_environ
from m4db_database.decorators import static

SQLITE_URI_STRING_01 = "sqlite:///{file:}"
SQLITE_DATABASE_TYPE = "SQLITE"


def setup_database(file, echo=False):
    r"""
    Create tables, indexes, relationships etc. for an SQLight database.

    Args:
        file: the file that will contain the database objects.
        echo: boolean (default False) set to True if verbose SQLAlchemy output is required.

    Returns:
        The url string to connect to the databse.
    """

    db_uri = SQLITE_URI_STRING_01.format(file=os.path.abspath(file))

    engine = create_engine(db_uri, echo=echo)

    if hasattr(m4db_database.orm.Base, "metadata"):
        metadata = getattr(m4db_database.orm.Base, "metadata")
        metadata.create_all(engine)
    else:
        raise AssertionError("Fatal, m4db_database.orm.Base has no attribute 'metadata'")

    return db_uri


@static(engine=None, Session=None)
def get_session(file=None, scoped=False, echo=False):
    r"""
    Retrieve an open database connection session, if file is None, then attempt to use config
    data stored in the file pointed to by the M4DB_CONFIG environment variable.

    Args:
        file: the file that will contain the database objects.
        scoped: if true return a 'scoped' session otherwise don't
        echo: boolean (default False) set to True if verbose SQLAlchemy output is required.

    Returns:
        A session connection to the database.
    """
    if get_session.engine is None:
        if file is None:
            config = read_config_from_environ()
            sql_uri = config["db_uri"]
        else:
            sql_uri = SQLITE_URI_STRING_01.format(file=file)
        get_session.engine = create_engine(sql_uri, echo=echo)

    if get_session.Session is None:
        get_session.Session = sessionmaker(
            bind=get_session.engine,
            autoflush=True,
            autocommit=False
        )

    if scoped:
        return scoped_session(get_session.Session)
    else:
        return get_session.Session()
