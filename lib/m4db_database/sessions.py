r"""
Set of utilities to open databases.
"""

from m4db_database.configuration import read_config_from_environ

from m4db_database.sessions_postgres import POSTGRES_DATABASE_TYPE
from m4db_database.sessions_sqlite import SQLITE_DATABASE_TYPE


def get_session(scoped=False, echo=False, nullpool=False):
    r"""
    Retrieve an SQLAlchemy open database connection session from the M4DB_CONFIG file.
    Parameters:
        scoped: if true we return a scoped session.
        echo: if true we return a session with echoing enabled.
        nullpool: if true we return a non-pooled session.
    Returns:
        A session connection.
    """
    config = read_config_from_environ()

    if config["db_type"] == POSTGRES_DATABASE_TYPE:
        from m4db_database.sessions_postgres import get_session
        return get_session(scoped=scoped, echo=echo, nullpool=nullpool)
    if config["db_type"] == SQLITE_DATABASE_TYPE:
        from m4db_database.sessions_sqlite import get_session
        return get_session(scoped=scoped, echo=echo)

    raise ValueError("Unsupported type in M4DB_CONFIG db_type: {}".format(config["db_type"]))
